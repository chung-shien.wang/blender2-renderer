import os 
os.environ["OPENCV_IO_ENABLE_OPENEXR"]="1"

import cv2 
import numpy as np 
import argparse

def depth_normalization(img, min_d = 0.1, max_d = 10):
    # min_d: near plane location, set to be 0.1
    # max_d: far plane location, set to be 10
    img_norm = img.copy()
    img_norm[img_norm > max_d] = max_d
    return (img_norm - min_d) / (max_d - min_d)

def post_processing(file_path):
    # INPUT: 
    #  3-channel 32-bit exr image of un-normalized depth
    # OUTPUT (save to the original directory): 
    #  1-channel 16-bit png image of normalized depth 
    if not os.path.exists(file_path):
       return 
    
    _, ext = os.path.splitext(file_path)
    if ext.lower() == '.exr':
        img = cv2.imread(file_path,  cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH) 
        gray_img = img[..., 0] if len(img.shape) >= 3 else img # transform to 1-channel 
        gray_img = depth_normalization(gray_img) * (2**16 - 1)
        gray_img = gray_img.astype(np.uint16)

        # save as png file
        save_path = file_path.replace('.exr', '.png')  
        cv2.imwrite(save_path, gray_img)

        # delete exr file 
        if save_path != file_path:
            os.system(f"rm -rf {file_path}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--depth', type=str, required=True, help='Path to the depth image')
    args = parser.parse_args()
    
    post_processing(args.depth)
