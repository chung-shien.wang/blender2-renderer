"""
Use run_render_depth.py to run this script.
"""
import subprocess
import argparse
import numpy as np
import os
import sys
sys.path.append(os.path.dirname(__file__))

import util
import bpy
import glob
from PIL import Image

class DepthInterface():
    def __init__(self, resolution=128, background_color=(1,1,1)):
        self.resolution = resolution

        # Delete the default cube (default selected)
        bpy.ops.object.delete()
        self.delete_object_and_data("model_normalized")

        # Deselect all. All new object added to the scene will automatically selected.
        self.blender_renderer = bpy.context.scene.render
        self.blender_renderer.use_antialiasing = False
        self.blender_renderer.resolution_x = resolution
        self.blender_renderer.resolution_y = resolution
        self.blender_renderer.resolution_percentage = 100
        self.blender_renderer.image_settings.file_format = 'PNG'  # set output format to .png

        self.blender_renderer.alpha_mode = 'SKY'

        world = bpy.context.scene.world
        world.horizon_color = background_color
        world.light_settings.use_environment_light = True
        world.light_settings.environment_color = 'SKY_COLOR'
        world.light_settings.environment_energy = 1.

        lamp1 = bpy.data.lamps['Lamp']
        lamp1.type = 'SUN'
        lamp1.shadow_method = 'NOSHADOW'
        lamp1.use_specular = False
        lamp1.energy = 1.

        bpy.ops.object.lamp_add(type='SUN')
        lamp2 = bpy.data.lamps['Sun']
        lamp2.shadow_method = 'NOSHADOW'
        lamp2.use_specular = False
        lamp2.energy = 1.
        bpy.data.objects['Sun'].rotation_euler = bpy.data.objects['Lamp'].rotation_euler
        bpy.data.objects['Sun'].rotation_euler[0] += 180

        bpy.ops.object.lamp_add(type='SUN')
        lamp2 = bpy.data.lamps['Sun.001']
        lamp2.shadow_method = 'NOSHADOW'
        lamp2.use_specular = False
        lamp2.energy = 0.3
        bpy.data.objects['Sun.001'].rotation_euler = bpy.data.objects['Lamp'].rotation_euler
        bpy.data.objects['Sun.001'].rotation_euler[0] += 90

        # Set up the camera
        self.camera = bpy.context.scene.camera
        self.camera.data.sensor_height = self.camera.data.sensor_width # Square sensor
        util.set_camera_focal_length_in_world_units(self.camera.data, 525./512*resolution) # Set focal length to a common value (kinect)

        bpy.ops.object.select_all(action='DESELECT')

    def import_mesh(self, fpath, scale=1., object_world_matrix=None):
        self.delete_object_and_data("model_normalized")
        ext = os.path.splitext(fpath)[-1]
        if ext == '.obj':
            bpy.ops.import_scene.obj(filepath=str(fpath), split_mode='OFF')
        elif ext == '.ply':
            bpy.ops.import_mesh.ply(filepath=str(fpath))

        obj = bpy.context.selected_objects[0]
        obj.name = "model_normalized"
        util.dump(bpy.context.selected_objects)

        if object_world_matrix is not None:
            obj.matrix_world = object_world_matrix

        bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY', center='BOUNDS')
        obj.location = (0., 0., 0.) # center the bounding box!

        if scale != 1.:
            bpy.ops.transform.resize(value=(scale, scale, scale))

        # Disable transparency & specularities
        M = bpy.data.materials
        for i in range(len(M)):
            M[i].use_transparency = False
            M[i].specular_intensity = 0.0

        # Disable texture interpolation
        T = bpy.data.textures
        for i in range(len(T)):
            try:
                T[i].use_interpolation = False
                T[i].use_mipmap = False
                T[i].use_filter_size_min = True
                T[i].filter_type = "BOX"
            except:
                continue
        
    def render(self, output_dir, blender_cam2world_matrices):
        depth_dir = os.path.join(output_dir, "depth")
        util.cond_mkdir(depth_dir)

        # save the paths to all the rendered masks for post processing 
        rendered_depth_paths = []

        for i in range(len(blender_cam2world_matrices)):
            self.camera.matrix_world = blender_cam2world_matrices[i]

            if os.path.exists(os.path.join(depth_dir, '%06d.png' % i)):
                continue

            self.set_up_depth(depth_dir)

            # Render the mask
            bpy.ops.render.render(write_still=False)  # write_still=False will not save the RGB image.

            # rename the mask image since it's given a default name by Blender
            file_list = glob.glob(os.path.join(depth_dir, 'Image*.exr'))
            if file_list:
                stupid_default_name = file_list[0]
                new_name = os.path.join(depth_dir, f'{i:06d}.exr')
                os.system(f"mv {stupid_default_name} {new_name}")
                rendered_depth_paths.append(new_name)
            
        # Remember which meshes were just imported
        meshes_to_remove = []
        for ob in bpy.context.selected_objects:
            meshes_to_remove.append(ob.data)

        bpy.ops.object.delete()

        # Remove the meshes from memory too
        for mesh in meshes_to_remove:
            bpy.data.meshes.remove(mesh)

        return rendered_depth_paths

    def set_up_depth(self, depth_dir):
        bpy.context.scene.render.layers.active.use_pass_z = True
        bpy.context.scene.use_nodes = True
        tree = bpy.context.scene.node_tree
        links = tree.links
        for node in tree.nodes:
            tree.nodes.remove(node)
        render_layer_node = tree.nodes.new(type='CompositorNodeRLayers')
        depth_output_node = tree.nodes.new(type='CompositorNodeOutputFile')
        depth_output_node.base_path = depth_dir
        # Set the output to OPEN_EXR format with 32-bit depth
        depth_output_node.format.file_format = 'OPEN_EXR'
        depth_output_node.format.color_depth = '32'
        try:
            links.new(render_layer_node.outputs['Z'], depth_output_node.inputs[0])
        except KeyError:
            links.new(render_layer_node.outputs['Depth'], depth_output_node.inputs[0])


    def delete_object_and_data(self, obj_name):
        # delete all objects except "Camera" and "Lamp"
        bpy.ops.object.select_all(action='DESELECT')
        # Loop through all objects and select them except for "Camera" and "Lamp"
        for obj in bpy.context.scene.objects:
            if obj.name not in ['Camera', 'Lamp']:
                obj.select = True
        # Delete selected objects
        bpy.ops.object.delete()
        
        bpy.ops.object.select_all(action='DESELECT')
        # Select the object
        if obj_name in bpy.data.objects:
            bpy.data.objects[obj_name].select = True
            # Delete the object
            bpy.ops.object.delete()
            # Remove mesh data
            if obj_name in bpy.data.meshes:
                bpy.data.meshes.remove(bpy.data.meshes[obj_name])

def get_subdirectories(base_dir, directory=True):
    if os.path.isdir(base_dir):
        return sorted([os.path.join(base_dir, d) for d in os.listdir(base_dir) if (not directory) or os.path.isdir(os.path.join(base_dir, d))])
    else:
        return []
    
def get_obj_path(obj, obj_dir):
    obj_name = obj.split('/')[-1]
    return os.path.join(obj_dir, obj_name, "models/model_normalized.obj")

def post_processing(depth_path):
    command = ["python", "post_processing_depth.py", "--depth", depth_path]
    try:
        subprocess.run(command, check=True)
    except subprocess.CalledProcessError as e:
        print(f"Error: {e}")

def render_object(renderer, poses, obj_path, output_dir):
    #parent_dir = os.path.join(output_dir, poses[0].split('/')[-3])
    parent_dir = os.path.join(output_dir, poses[0].split('/')[-4], poses[0].split('/')[-3])
    # render pipeline
    obj_location = np.zeros((1, 3))
    cv_poses = [util.read_pose(pose) for pose in poses]
    blender_poses = [util.cv_cam2world_to_bcam2world(m) for m in cv_poses]
    rendered_depth_paths = renderer.render(parent_dir, blender_poses)

    # post processing: convert 4-channel to 1-channel
    for depth in rendered_depth_paths:
        post_processing(depth)

p = argparse.ArgumentParser()
p.add_argument('--img_dir', type=str, required=True)
p.add_argument('--output_dir', type=str, required=True)
p.add_argument('--obj_dir', type=str, required=True)
p.add_argument('--obj', type=str, required=True)
argv = sys.argv
argv = sys.argv[sys.argv.index("--") + 1:]
opt = p.parse_args(argv)

img_dir = opt.img_dir
output_dir = opt.output_dir
obj_dir = opt.obj_dir
obj = opt.obj

objects = sorted(get_subdirectories(img_dir))
total_number = len(objects)

pose_path = os.path.join(obj, 'pose')
poses = sorted(get_subdirectories(pose_path, directory=False))
obj_path = get_obj_path(obj, obj_dir)

rendered_depths_path = os.path.join(output_dir, img_dir.split('/')[-1], pose_path.split('/')[-2], 'depth')
rendered_depths = get_subdirectories(rendered_depths_path, directory=False)
poses_count, depths_count = len(poses), len(rendered_depths) # number of poses to render, number of rendered masks
if poses_count == depths_count: 
    print('This folder is already rendered. Just do post-processing')
    for depth in rendered_depths:
        post_processing(depth)
else: 
    renderer = DepthInterface(resolution=128)
    renderer.import_mesh(obj_path, scale=1., object_world_matrix=None)
    render_object(renderer, poses, obj_path, output_dir)