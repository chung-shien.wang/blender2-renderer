#!/bin/bash

# test car
obj_dir="/home/wang_c/Downloads/shapenet_part/1a0bc9ab92c915167ae33d942430658c/models/model_normalized.obj"

output_dir="/home/wang_c/Downloads/buffer"
num_observations=10
sphere_radius=1
resolution=1024
mode="train"

rm -rf $output_dir
mkdir $output_dir
blender --background --python shapenet_spherical_renderer.py -- --output_dir "$output_dir" --mesh_fpath "$obj_dir" --num_observations "$num_observations" --sphere_radius "$sphere_radius" --resolution="$resolution" --mode="$mode" 