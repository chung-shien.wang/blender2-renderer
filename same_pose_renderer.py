"""
Run this script using:

    blender -b --python same_pose_renderer.py 
    
"""
import argparse
import numpy as np
import os
import sys
sys.path.append(os.path.dirname(__file__))

import util
import blender_interface

obj_dir = '/globalwork/datasets/ShapeNetCore.v2/02958343/1a1dcd236a1e6133860800e6696b8284/models/model_normalized.obj'
pose_dir = '/home/wang_c/Downloads/SRN_part/1a1dcd236a1e6133860800e6696b8284/pose/000000.txt'
intrinsic_dir = ''
output_dir = '/home/wang_c/Downloads/buffer2'

os.system(f'rm -rf {output_dir}')
os.system(f'mkdir {output_dir}')


renderer = blender_interface.BlenderInterface(resolution=128)

obj_location = np.zeros((1,3))

# read camera pose from txt file 
cam_pose = util.read_pose(pose_dir)

### test ###
#  To set the obj matrix to identity, we should also change the cam_pose. 
#  (rotating it by -90 deg along x-axis)
cam_pose = np.array([
    [1, 0, 0, 0],
    [0, 0, 1, 0],
    [0, -1, 0, 0],
    [0, 0, 0, 1]]) @ cam_pose

blender_poses = [util.cv_cam2world_to_bcam2world(cam_pose)]

obj_pose = np.eye(4)

renderer.import_mesh(obj_dir, scale=1., object_world_matrix=obj_pose)
renderer.render(output_dir, blender_poses, write_cam_params=True)