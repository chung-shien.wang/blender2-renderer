"""
Render mask for a object
To use: 
    (nohup) python run_render_depth.py
"""
import argparse
import os
import sys
import subprocess
import cv2
sys.path.append(os.path.dirname(__file__))

if os.path.exists("stop"):
    os.system("rm stop")

def get_subdirectories(base_dir, directory=True):
    if os.path.isdir(base_dir):
        return [os.path.join(base_dir, d) for d in os.listdir(base_dir) if (not directory) or os.path.isdir(os.path.join(base_dir, d))]
    else:
        return []

# cars_train id: 02958343 | total: 2458 * 50
# cars_val id: 02958343 | total: 352 * 250 
# # cars_test id: 02958343 | total: 704 * 250 
# img_dir = '/nodes/bosch/fastwork/schmidt/SRN_Dataset/cars_val'
# output_dir = '/nodes/astra/fastwork/wang_c/SRN_depth'
# obj_dir = '/globalwork/datasets/ShapeNetCore.v2/02958343'

# chairs_train id: 03001627 | total: 4612 * 50 = 208100 (currently ~16 "folders" per minute)
# img_dir = "/nodes/bosch/fastwork/schmidt/SRN_Dataset/chairs_train/chairs_2.0_train"
"""output folder need to be renamed"""
# chair_val id: 03001627 | total: 662 * 250 = 165500
# img_dir = "/nodes/bosch/fastwork/schmidt/SRN_Dataset/chairs_val"
# chairs_test id: 03001627 | total: 1317 * 250 = 329250
img_dir = "/nodes/bosch/fastwork/schmidt/SRN_Dataset/chairs_test"
output_dir = '/nodes/astra/fastwork/wang_c/SRN_depth'
obj_dir = '/globalwork/datasets/ShapeNetCore.v2/03001627'

objects = sorted(get_subdirectories(img_dir))
total_number = len(objects)


for i, obj in enumerate(objects): 
    #os.system("clear")
    print('='*60)
    print(f"The {i+1}/{total_number} instance")
    print('-'*50)

    command = [
        "blender", "-b", "--python", "render_depth.py", "--", 
        "--img_dir", img_dir, 
        "--output_dir", output_dir,
        "--obj_dir", obj_dir, 
        "--obj", obj]
    try:
        subprocess.run(command, check=True)
    except subprocess.CalledProcessError as e:
        print(f"Error: {e}")
        
    # "touch stop" to stop the program 
    if os.path.exists("stop"):
        print("Stop file found. Exiting.")
        break

